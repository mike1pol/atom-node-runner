'use babel';

import {
    $
} from 'atom-space-pen-views';

export default class TerminalView {
    constructor(id) {
        this.id = id;
        this.content();
        this.panel = atom.workspace.addBottomPanel({
            item: this.getContent(),
            priority: 100,
            visible: false
        });
    }

    content() {
        this.div = $('<div class="node-runner terminal-view"></div>');
        this.panelDivider = $('<div class="panel-divider"></div>');
        this.panelDivider.appendTo(this.div);
        this.btnToolbar = $('<div class="btn-toolbar"></div>');
        this.closeBtn = $('<i class="btn icon icon-x right"></i>');
        this.closeBtn.appendTo(this.btnToolbar);
        this.hideBtn = $('<i class="btn icon icon-chevron-down right"></i>');
        this.hideBtn.appendTo(this.btnToolbar);
        this.startBtn = $('<i class="btn icon icon-triangle-right left"></i>');
        this.startBtn.appendTo(this.btnToolbar);
        this.stopBtn = $('<i class="btn icon icon-primitive-square left"></i>');
        this.stopBtn.appendTo(this.btnToolbar);
        this.btnToolbar.appendTo(this.div);
        this.xterm = $(`<div class="xterm" id="${this.id}"></div>`);
        // this.terminal = $('<div class="terminal"></div>');
        // this.terminal.appendTo(this.xterm);
        this.xterm.appendTo(this.div);
    }

    getContent() {
        return this.div;
    }
}
