'use babel';

import {
    $
} from 'atom-space-pen-views';

export default class BottomView {
    constructor(menu, closeAll, activeBar) {
        this.activeBar = activeBar;
        this.div = $('<div class="node-runner status-bar" tabindex="-1" is="space-pen-div"></div>');
        this.menu = $('<i class="icon icon-triangle-right"></i>');
        if (menu) {
            this.menu.bind('click', menu);
        }
        this.menu.appendTo(this.div);
        this.list = $('<ul class="list-inline status-container" tabindex="-1" is="space-pen-ul"></ul>"');
        this.list.appendTo(this.div);
        this.closeAll = $('<i class="icon icon-x"></i>');
        if (closeAll) {
            this.closeAll.bind('click', closeAll);
        }
        this.closeAll.appendTo(this.div);
    }

    getContent() {
        return this.div;
    }

    appendList(item) {
        $('li', this.list).removeClass('active');
        const src = (item.type === 'npm') ? 'atom://node-runner/resources/npm.png' : 'atom://node-runner/resources/node.png';
        const element = $(`<li class="status-list active" data-path="${item.path}" id="${item.id}"><img border="0" src="${src}"> ${item.title}</li>`);
        const self = this;
        element.bind('click', function Activate() {
            const id = $(this).attr('id');
            self.activeBar(id);
        });
        element.appendTo(this.list);
    }
}
