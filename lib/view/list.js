'use babel';

import { SelectListView } from 'atom-space-pen-views';

export default class ListView extends SelectListView {
    constructor() {
        super();
        this.addClass('from-top');
        this.panel = atom.workspace.addModalPanel({
            item: this,
            visible: false
        });
    }

    show() {
        const items = [
            {
                title: 'npm scripts',
                cmd: 'npm'
            },
            {
                title: 'run current file',
                cmd: 'currentFile'
            }
        ];
        this.setItems(items);
        this.panel.show();
    }

    hide() {
        this.panel.hide();
    }

    viewForItem(item) {
        return `<li>${item.title}</li>`;
    }

    cancelled() {
        this.panel.hide();
    }
}
