'use babel';

import path from 'path';
import fs from 'fs';
import { SelectListView } from 'atom-space-pen-views';

export default class NPMView extends SelectListView {
    constructor() {
        super();
        this.addClass('from-top');
        this.panel = atom.workspace.addModalPanel({
            item: this,
            visible: false
        });
    }

    show() {
        let items = [];
        atom.project.getDirectories().forEach(d => {
            const realPath = (d.realPath) ? d.realPath : d.path;
            const packagePath = path.join(realPath, 'package.json');
            if (fs.existsSync(packagePath)) {
                const basePath = path.parse(realPath);
                try {
                    const Package = JSON.parse(fs.readFileSync(packagePath).toString());
                    items.push({
                        title: basePath.name,
                        path: realPath,
                        scripts: Package.scripts
                    });
                } catch (e) {
                    console.error(e);
                }
            }
        });
        if (items.length === 1) {
            items = Object.keys(items[0].scripts).map(v => ({
                title: v,
                cmd: items[0].scripts[v],
                path: items[0].path
            }));
        }
        this.setItems(items);
        this.panel.show();
    }

    hide() {
        this.panel.hide();
    }

    viewForItem(item) {
        return `<li>${item.title} ${item.cmd ? `<br><small>${item.cmd}</small>` : ''}</li>`;
    }

    cancelled() {
        this.panel.hide();
    }
}
