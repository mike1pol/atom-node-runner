'use babel';


import path from 'path';
import { exec } from 'child_process';
import {
    $
} from 'atom-space-pen-views';
import {
    CompositeDisposable
} from 'atom';

import ansiHtml from 'ansi-html-stream';

import ListView from './view/list';
import NPMView from './view/npm';
import BottomView from './view/bottom';
import TerminalView from './view/terminal';

export default {

    listView: null,
    npmView: null,
    bottomView: null,
    terminalView: null,
    running: {},
    active: null,
    subscriptions: null,
    test: null,

    activate() {
        this.listView = new ListView();
        this.listView.confirmed = (item) => {
            this.listView.hide();
            this[item.cmd]();
        };

        this.npmView = new NPMView();
        this.npmView.confirmed = (item) => {
            this.npmView.hide();
            if (item.scripts) {
                this.npmView.setItems(Object.keys(item.scripts).map(v => ({
                    title: v,
                    cmd: item.scripts[v],
                    path: item.path
                })));
                this.npmView.panel.show();
                return this.npmView.focusFilterEditor();
            }
            const id = `npm${item.path.replace(/\//g, '-')}${item.title}`;
            if (!this.running[id] || !this.running[id].programm) {
                this.active = id;
                if (!this.running[id]) {
                    this.bottomView.appendList({
                        id,
                        type: 'npm',
                        title: item.title,
                        path: item.path
                    });
                    this.running[id] = {
                        htmlStream: ansiHtml(),
                        programm: null,
                        cwd: item.path,
                        cmd: `npm run ${item.title}`,
                        view: $(`<div id="terminal_${id}" style="display:none;" class="terminal"></div>`)
                    };
                    this.running[id].htmlStream.id = id;
                    this.running[id].htmlStream.on('data', data => {
                        console.log(data);
                        $(data).appendTo(this.running[this.id].view);
                    });
                }
                try {
                    this.running[id].programm = exec(this.running[id].cmd, {
                        stdio: 'pipe',
                        env: process.env,
                        cwd: this.running[id].cwd
                    });
                    this.running[id].programm.stdout.pipe(this.running[id].htmlStream);
                    this.running[id].programm.stderr.pipe(this.running[id].htmlStream);
                    this.running[id].programm.once('exit', code => {
                        console.log('exit', code);
                        this.running[id].programm = null;
                    });
                    this.running[id].programm.on('error', err => {
                        $(`<div>${err.message}</div>`).appendTo(this.running[id].view);
                    });
                } catch (err) {
                    this.running[id].programm = null;
                    $(`<div>${err.message}</div>`).appendTo(this.running[id].view);
                }
                this.running[id].view.appendTo(this.terminalView.xterm);
            }
            this.activeBar(id);
            const visible = this.terminalView.panel.isVisible();
            if (!visible) {
                this.terminalView.panel.show();
            }
            return console.log(`run cmd *${item.title}* on path ${item.path}`);
        };

        this.bottomView = new BottomView(
            () => {
                this.menu();
            },
            () => {
                this.closeAll();
            },
            (id) => {
                this.activeBar(id);
            }
        );
        this.bottomPanel = atom.workspace.addBottomPanel({
            item: this.bottomView.getContent(),
            priority: 100
        });

        this.terminalView = new TerminalView('terminalViewId');
        // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
        this.subscriptions = new CompositeDisposable();

        // Register command that toggles this view
        this.subscriptions.add(atom.commands.add('atom-workspace', {
            'node-runner:menu': () => this.menu(),
            'node-runner:npm': () => this.npm(),
            'node-runner:currentFile': () => this.currentFile()
        }));
    },

    changeTerminalStatus(status) {
        if (status) {
            this.terminalView.startBtn.hide();
            this.terminalView.stopBtn.show();
        } else {
            this.terminalView.stopBtn.hide();
            this.terminalView.startBtn.show();
        }
    },

    activeBar(id) {
        console.log(this.bottomView.list);
        console.log($(`#${id}`, this.bottomView.list));
        $('li', this.bottomView.list).removeClass('active');
        $(`#${id}`, this.bottomView.list).addClass('active');
        $('.terminal', this.terminalView.xterm).hide();
        $(`#terminal_${id}`, this.terminalView.xterm).show();
        this.active = id;
        console.log('active ', id);
    },

    closeAll() {
        $(this.bottomView.list).html('');
        this.running = {};
    },

    deactivate() {
        this.subscriptions.dispose();
        this.listView.destroy();
        this.npmView.destroy();
    },

    menu() {
        const visible = this.listView.panel.isVisible();
        if (visible) {
            this.listView.panel.hide();
        }
        this.listView.show();
        return this.listView.focusFilterEditor();
    },

    npm() {
        const visible = this.npmView.panel.isVisible();
        if (visible) {
            this.npmView.panel.hide();
        }
        this.npmView.show();
        return this.npmView.focusFilterEditor();
    },

    currentFile() {
        const editor = atom.workspace.getActivePaneItem();
        if (editor && editor.buffer.file && editor.buffer.file.path) {
            const filePath = editor.buffer.file.path;
            const basePath = path.parse(filePath);
            const title = basePath.name;
            const id = `node${filePath.replace(/\//g, '-').replace(/\./g, '-')}`;
            if (this.running.indexOf(id) === -1) {
                this.running.push(id);
                this.bottomView.appendList({
                    id,
                    type: 'node',
                    title,
                    path: filePath
                });
            } else {
                this.activeBar(id);
            }
        }
    }
};
