# [node-runner](https://github.com/mike1pol/atom-node-runner)

npm & node.js runner

## Version 1.0.0
- [x] Create menus
- [x] Create bottom panel
- [x] Create output view
- [ ] Run npm scripts
- [ ] Run node.js file
- [ ] Create node.js run
- [ ] Run node.js from list
